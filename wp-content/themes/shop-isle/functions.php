<?php
/**
 * Main file
 */

/**
 * Initialize all the things.
 */
require get_template_directory() . '/inc/init.php';

/**
 * Note: Do not add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * http://codex.wordpress.org/Child_Themes
 */


 require 'inc/cwp-update-free.php';



// enable svg uploads
 add_filter('upload_mimes', 'custom_upload_mimes');

function custom_upload_mimes ( $existing_mimes=array() ) {

	// add the file extension to the array

	$existing_mimes['svg'] = 'mime/type';

        // call the modified list of extensions

	return $existing_mimes;
}

// remove open graph images from jetpack as it provides a blank image
add_filter( 'jetpack_enable_open_graph', '__return_false', 99 );
