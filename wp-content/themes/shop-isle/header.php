<?php
/**
 * The header for our theme.
 *
 * @package shop-isle
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> <?php shop_isle_html_tag_schema(); ?>>
<head>
<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon-194x194.png" sizes="194x194">
<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#ffd700">
<meta name="apple-mobile-web-app-title" content="Beauva">
<meta name="application-name" content="Beauva">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<meta property="og:site_name" content="Beauva">
<meta property="og:title" content="Beauva" />
<meta property="og:description" content="The Fashion Voyage" />
<meta property="og:image" itemprop="image" content="http://beauva.com/beauvalogo-256x256.png">
<meta property="og:type" content="website" />
<meta property="og:updated_time" content="1440432930" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<link itemprop="thumbnailUrl" href="http://beauva.com/beauvalogo-256x256.png">
<span itemprop="thumbnail" itemscope itemtype="http://schema.org/ImageObject">
  <link itemprop="url" href="http://beauva.com/beauvalogo-256x256.png">
</span>

	<a class="get_on_gplay" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/get_it_on_google_play.png" alt="Get Beauva on playstore" /></a>

	<?php do_action( 'shop_isle_before_header' ); ?>

	<!-- Preloader -->
	<?php

	global $wp_customize;

	/* Preloader */
	if(is_front_page() && !isset( $wp_customize ) && get_option( 'show_on_front' ) != 'page' ):

		$shop_isle_disable_preloader = get_theme_mod('shop_isle_disable_preloader');

		if( isset($shop_isle_disable_preloader) && ($shop_isle_disable_preloader != 1) ):

			echo '<div class="page-loader">';
				echo '<div class="loader">'.__('Loading...','shop-isle').'</div>';
			echo '</div>';

		endif;

	endif;



	?>

	<?php do_action( 'shop_isle_header' ); ?>

	<?php do_action( 'shop_isle_after_header' ); ?>
